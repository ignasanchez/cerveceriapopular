import React from 'react';
import { BrowserRouter as Router, Route } from 'react-router-dom';
import AuthContextProvider from './contexts/AuthContext';
import PrivateRoute from './PrivateRoute';
import backdoor from './components/backdoor';
import Dashboard from './components/dashboard';
import Menu from './components/menu-new'
import './App.sass';

function App() {
  return (
      <div className="App">
        <Router>
        <AuthContextProvider>
          <Route path='/' component={Menu} exact/>
          <Route path="/backdoor" component={backdoor} />
          <PrivateRoute exact path="/dashboard"y component={Dashboard} />
        </AuthContextProvider>
        </Router>
      </div>
  );
}

export default App;
