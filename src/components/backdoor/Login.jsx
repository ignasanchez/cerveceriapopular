import React, { useState } from 'react';
import { motion } from 'framer-motion';
import app from '../../firebase';
import logo from '../../assets/logo.png';
import './backdoor.css';

const Login = ({ history }) => {
    const [user, setUser] = useState({
        email: '', password: ''
    });
    const handleChange = (e) => {
        setUser({
            ...user, [e.target.name]: e.target.value
        })
    }
    const login = async (e) => {
        e.preventDefault()
        //firebase login
        try {
            await app.auth()
                .signInWithEmailAndPassword(user.email, user.password);    
            history.push('/dashboard');    

        } catch(error) {
            alert(error)
        }
    }
        
    return (
        <motion.div 
            className='columns py-5 px-5'
            //framer motion Animations
            initial={{y: 300, opacity: 0}}
            animate={{y: 0, opacity: 1}}
            //framer motion Animations
        >
            <div className='column is-one-quarter-desktop'>
            <p className='image is-128x128'>
                <img id='logo' src={logo} alt="cerveceriapopular"/>
            </p>
            <h1 className='is-size-3'> Acceso Administración </h1>
            <div className='field'>
                <label htmlFor="user" className='label'>
                    usuario :
                </label>
                <div className="control">
                    <input 
                        id='user' 
                        className="input is-rounded" 
                        type='email' 
                        name='email'
                        placeholder="encargado@cerveceriapopular.com.ar"
                        onChange={handleChange}
                    />
                </div>
            </div>
            <div className="field">
                <label htmlFor="password" className='label'>
                    contraseña :
                </label>
                <div className="control">
                    <input 
                        id="password"
                        className='input is-rounded'
                        type="password" 
                        name="password"
                        placeholder='********'
                        onChange={handleChange}
                    />
                </div>
            </div>
            <div className="control py-2">
                <a href="/dashboard" onClick={login} className="button is-primary is-medium is-fullwidth is-rounded">
                    Iniciar Sesión
                </a>
            </div>
            </div>
        </motion.div>
        );
}
 
export default Login;
