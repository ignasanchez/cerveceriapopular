import React, { useContext } from 'react';
import { motion } from 'framer-motion';
import logo from '../../assets/logo.png';
import { AuthContext } from '../../contexts/AuthContext';

const navStyles ={
    alignItems: 'center',
    display: 'flex',
    justifyContent: 'space-between'
}

const Nav = () => {
    const { logOut } = useContext(AuthContext);
    return (
        <motion.nav 
            initial={{y: -100, opacity: 0}}
            animate={{y: 0, opacity: 1}}
            className="navbar is-transparent" 
            style={navStyles}
        >
            <div className='navbar-brand image is-96x96'>
                <img src={logo} alt="cerveceria popular"/>
            </div>
            <button  
                className="button is-primary is-rounded"
                onClick={logOut}
            >cerrar sesión</button>
        </motion.nav>
    );
}
 
export default Nav;
