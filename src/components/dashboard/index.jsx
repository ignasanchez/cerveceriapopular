import React, { useState } from 'react';
import { useDocumentOnce } from 'react-firebase-hooks/firestore';
import { db } from '../../firebase';
import Nav from './Nav';
import AddOption from './AddOption'


const Dashboard = () => {
    // Query indexes database .
    // NOW in order to get the menu indexes we just get one read.
    const [index, loading, error] = useDocumentOnce(
        db.doc('indexes/kTwS9FRxL7oL9WmBWLvP')
    )
    // Setting indexes Variable to later be mapped in select component
    const indexes = index !== undefined && index.data()
    
    // Set select functionality
    const [section, setSection] = useState(null);
    const handleSelect = (e) => {
        setSection(e.target.value)
    }

    return (
    <section className='py-5 px-5'>
        { error && console.log(error)}
        <Nav />
        <div className='columns'>
            <div className="column is-4">
                <h4 className='subtitle is-size-5'>Bienvenido</h4>
                <h1 className='title is-size-3 is-primary'>
                Agregá o edita platos en tu carta!
                </h1>
                { loading && (
                    <progress className="progress is-small" max="100">50%</progress>
                )}
                { index &&  ( 
                    <>
                    <h4 className='subtitle'>selecciona una sección :</h4>
                    <div className="select is-medium is-rounded">
                        <select onChange={handleSelect}>
                            { indexes['data'].map(
                                index => (
                                    <option 
                                        key={ index.path }
                                        value={ index.path }
                                    >
                                        { index.section }
                                    </option>
                                )
                            )}
                        </select>
                    </div>
                    </>
                )}

            </div>
            { section !== null && (
                <AddOption path={section} />
            )}
        </div>
    </section>
    );
}

export default Dashboard;
