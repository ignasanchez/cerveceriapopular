import React, { useState } from 'react';
import { useDocumentData } from 'react-firebase-hooks/firestore';
import { AnimatePresence, motion } from 'framer-motion'
import { db } from '../../firebase';
import OptionForm from './OptionForm';

const AddOption = ({ path }) => {
    //Set state for form showing
    const [showForm, setShowForm] = useState(false);    
    // setting Query to database
    const query = `/menu_options/${path}`
    const [section, loading, error] = useDocumentData(
       db.doc( query )
    );
    // Up to now, the section constant is the document from the db
    // that we want to update. But beware! the asignment of this
    // constant is asynchronus.

    // Save the section options separately for convinience at the
    // time of editing.
    const options = section !== undefined && section.options;

    const [optionToUpdate, setOptionToUpdate] = useState();

    const addOption = () => {
        setOptionToUpdate();
        setShowForm(true)
    }

    const setUpdate = (id) => {
        const filteredOptions = options.filter(el => el.id === id);
        setOptionToUpdate(filteredOptions[0]);
        setShowForm(true);
    }

    return (
        <AnimatePresence>
        { error && console.log(error)}
        { loading && null}
        { section && (
            <>
            <motion.section
                // Framer motion Animations
                initial={{x: 200, opacity: 0}}
                animate={{x: 0, opacity: 1}}
                exit={{x: 200, opacity: 0}}
                // Framer motion Animations 
                key={path} className="column is-half" 
            >
            {/** OPTIONS-TABLE */}
                <table className="table is-fullwidth is-striped is-hoverable is-clickable">
                    <thead>
                        <tr>
                            <th> Opción </th>
                            <th> Descripción </th>
                            <th> Precio </th>
                            <th> Faltante </th>
                        </tr>
                    </thead>
                    <tbody>
                    {
                        options.map( (option) => (
                            <tr 
                                key={option.id}
                                onClick={() => setUpdate(option.id)}
                            >
                                <td>{option.name}</td>
                                <td>{option.desc}</td>
                                <td>{option.price}</td>
                                <td>
                                    { option.isMissing ? 'Faltante' : 'en Stock' }
                                </td>
                            </tr>
                        ))
                    }
                    <tr className='is-selected' onClick={addOption}>
                        <td ></td>
                        <td >+ Nueva Opción </td>
                        <td ></td>
                        <td ></td>
                    </tr>
                    </tbody>
                </table>
            </motion.section>
            {/** MODAL-FORM */
            showForm &&
            (<div className="modal is-active">
                <div className="modal-background"></div>
                <div className="modal-content">
                        <OptionForm 
                            option={optionToUpdate} 
                            section={section} 
                            path={path}
                            close={showForm}
                            setClose={setShowForm}
                        /> 
                </div>
            </div>)
            }
            </>
        )}
        </AnimatePresence>
    );
}
 
export default AddOption;
