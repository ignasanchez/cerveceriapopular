import React, { useState } from 'react';
import { findIndex } from 'lodash'
import { v4 as uuid } from 'uuid';
import { db } from '../../firebase';


const OptionForm = ({option, section, path, close, setClose }) => {
    //Reference to document
    const[descInput, setDescInput] = useState(false);
    const toggleDescInput = (e) => {
        e.preventDefault();
        setDescInput(!descInput);
    }
    // Initial model will check if the option prop is passed down
    // if it is will use the values of this to either add or edit 
    // the option to the menu. 
    const initial = {
        id: option ? option.id : uuid(),
        name: option ? option.name : '',
        desc: option ? option.desc :'',
        price: option ? option.price : '',
        isMissing: option ? option.isMissing : false
    }

    const [model, setModel] = useState(initial);
    // handling inputs
    const handleChange= (e) => {
        setModel({
            ...model,
            [e.target.name]: e.target.value
        })
    }
    // handling checkbox
    const handleCheckbox = (e) => {
        setModel({
            ...model,
            isMissing: !model.isMissing
        })
    }

/*     const saveChanges = (opt) => {
        //e.preventDefault();
        const sectionRef = db.collection('menu_options').doc(path);
        const newSection = {
            ...section,
            options: [...opt]
        };
        //Saving to db
        sectionRef.set(newSection, { merge: true })
        console.log('saving changes')
    }; */
    
    // submiting form
    const handleSubmit = (e) => {
        e.preventDefault();
        if( model.name !== '') {
            //setModel(initial);
            const sectionRef = db.collection('menu_options').doc(path);
            const index = findIndex(section.options, (o) => o.id === model.id);
            if (index >= 0) {
                const replace = section.options.splice(index, 1, model);
                sectionRef.set(section, { merge: true })
                console.log(replace)
            } else {
                section.options.push(model);
                sectionRef.set(section, { merge: true})
            }
        }
        setModel(initial);
        setClose(!close)
    }

    const deleteOption = (e) => {
        e.preventDefault()
        const sectionRef = db.collection('menu_options').doc(path);
        const index = findIndex(section.options, (o) => o.id === model.id)
        section.options.splice(index, 1)
        sectionRef.set(section, { merge: true });

        setModel(initial);
        setClose(!close);
    }

    return (

        <div className="modal-card  px-5 py-5">
            <header class="modal-card-head">
                <p class="modal-card-title">Agrega o edita</p>
                <button 
                    class="delete" 
                    aria-label="close"
                    onClick={()=> setClose(!close)}
                ></button>
            </header>
            <div className="modal-card-body">
                <div className="field">
                    <label htmlFor="name" className="label is-medium">
                        Nombre : 
                    </label>
                    <div className="control">
                        <input 
                            className='input is-rounded' 
                            name="name" id="name"
                            type="text"  
                            placeholder='Nombre de la opcion'
                            value={model.name} 
                            onChange={handleChange} 
                        />
                    </div>
                </div>
                <div className="field">
                    <label htmlFor="price" className="label is-medium mx-small">
                        Precio : 
                    </label>
                    <div className="control">
                        <input 
                            className='input is-rounded' 
                            type="text" 
                            value={model.price}
                            onChange={handleChange}
                            placeholder='Precio'
                            name="price" id="price"
                        />
                    </div>
                </div>
                <div className="field">
                    <label htmlFor="isMissing" className="checkbox">
                        Esta en faltantes? 
                        <input 
                            onChange={handleCheckbox}
                            checked={model.isMissing}
                            type="checkbox" 
                            name="isMissing" id="isMissing"
                        />
                    </label>
                </div>
                { descInput && (
                <div className="field">
                    <label htmlFor="desc" className="label is-medium">
                        descripcion (opcional) : 
                    </label>
                    <div className="control">
                        <textarea 
                            className='textarea is-rounded' 
                            value={model.desc}
                            onChange={handleChange}
                            placeholder='Descripcion de la opcion..'
                            name="desc" id="desc"
                        />
                    </div>
                </div>
            )}
                <footer className="card-footer">
                    <a 
                        onClick={handleSubmit}
                        href='/#' 
                        className='card-footer-item'
                    > Guardar </a>
                    <a 
                        onClick={toggleDescInput}
                        href="/#" 
                        className='card-footer-item'
                    >
                        { descInput ? 'Ocultar descripción' : 'Agregar descripción'} 
                    </a>
                    <a 
                        onClick={deleteOption}
                        href="/#" 
                        className='card-footer-item'
                    >
                        Eliminar
                    </a>
                </footer>
            </div>
        </div>
    );
}
 
export default OptionForm;
