import React from 'react'
import { useCollectionDataOnce } from 'react-firebase-hooks/firestore'
import { motion} from 'framer-motion'
import { db } from '../../firebase'
import logo from '../../assets/logo.png'
import { specials, coffes, sandwiches, beers, tables, filterCollection } from '../../utils'
import Acordion from './MenuAcordion'

const Menu = () => {
  const [collection, loading, error] = useCollectionDataOnce(
    db.collection('menu_options')
	)
	//ordenando la data de la coleccion
	const infoDiscos = collection && filterCollection(collection, specials);
	const infoCafeteria = collection && filterCollection(collection, coffes);
	const infoSandwiches = collection && filterCollection(collection, sandwiches);
	const infoCervezas = collection && filterCollection(collection, beers);
	const infoTablas = collection && filterCollection(collection, tables);
	const infoPostres = collection && filterCollection(collection, [{name: 'Postres'}])
	const infoMenu = collection && filterCollection(collection, [{name:'Menu'}])

	//filtrando opciones del Menu
	const sugerencias = infoMenu && infoMenu[0].options.filter( el => el.isMissing === false )
	
	return (
    <div className='section'>
		{ error && console.log(error) }
		<motion.header 
			initial={{scale: .5, opacity: 0}}
			animate={{scale: 1, opacity: 1}}
			transition={{delay: .5}}
			className='level py-5'
		>
			<div className="level-item has-text-centered">
					<img src={logo} width='128'alt="Cerveceria Popular"/>
			</div>
		</motion.header>
		<div className="container">
    { loading && <progress className="progress is-small" max="100">50%</progress> }
		{	collection && (
			<section className="columns is-multiline">
				<div className="column is-half">
					{ /* Menu del día */
						sugerencias.length > 1 && (
							<Acordion 
							section={infoMenu[0]} 
							options={sugerencias}
							/>
						)
					}
					{ /* Cafeteria */ }
					<Acordion 
						section={infoCafeteria[0]} 
						options={
							[	...infoCafeteria[0].options,
								...infoCafeteria[1].options,
								...infoCafeteria[2].options	] 
					} />
					<Acordion 
						section={ infoCafeteria[3] }
						options={ infoCafeteria[3].options }
					/>
				</div>
				<div className="column is-half">
					<motion.div 
						initial={{x: 200, opacity: 0}}
						animate={{x: 0, opacity: 1}}
						className="has-text-right"
					>
            <h1 className='subtitle is-size-1'>
              Nuestra Propuesta.
            </h1>
          </motion.div>
					{ /* PASTAS al Disco */	}
					<Acordion 
						section={ infoDiscos[1] }
						options={ infoDiscos[1].options }
						sauces={ infoDiscos[3]}
					/>
					{ /* CARNES al Disco */	}
					<Acordion 
						section={ infoDiscos[0] }
						options={ infoDiscos[0].options }
						sauces={ infoDiscos[3]}
						sides={ infoDiscos[2]}
					/>
				</div>
				<div className="column is-half">
				{	// TABLAS DE PICADA
					React.Children.toArray(infoTablas.map(
						item => item.options.length > 0 && (
							<Acordion section={item} options={item.options} />
						)
				))}
				{	// OTRAS OPCIONES
					React.Children.toArray(collection.map(
						item => item.options.length > 0 && (
							<Acordion section={item} options={item.options} />
						)
				))}
				</div>
				<div className="column is-half">
					<motion.div 
						initial={{x: -200, opacity: 0}}
						animate={{x: 0, opacity: 1}}
						className="has-text-justified"
					>
            <h1 className='subtitle is-size-1'>
              SANDWICHES
            </h1>
          </motion.div>
					{ React.Children.toArray(infoSandwiches.map(
						sandwich => <Acordion section={sandwich} options={sandwich.options}/>
					))}
				</div>
				<div className="column is-half">
					<motion.div 
						initial={{x: -200, opacity: 0}}
						animate={{x: 0, opacity: 1}}
						className="has-text-right"
					>
            <h1 className='subtitle is-size-1'>
              Bebidas
            </h1>
          </motion.div>
					{ React.Children.toArray(infoCervezas.map(
						cerveza => <Acordion section={cerveza} options={cerveza.options}/>
					))}
					<Acordion 
						section={infoCafeteria[1]} 
						options={infoCafeteria[1].options}
					/>
				</div>
				<div className="column is-half">
					<Acordion 
						section={infoPostres[0]}
						options={infoPostres[0].options}
					/>
				</div>
			</section>
		)}
			{ /*FOOTER FOOTER FOOTER*/}
			<div className="has-text-centered">
          <small>servicio de cubierto $50</small> <br/>
          <strong>Lunes a Domingos de 9:30 a 23:30hs.</strong>
      </div>
		</div>
    </div>
  );
}
 
export default Menu;
