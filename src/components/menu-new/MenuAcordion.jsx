import React, { useState } from 'react'
import { v4 as uuid } from 'uuid'
import { AnimatePresence, motion } from 'framer-motion'
import styles from './Menu.module.css'

const ListItem = ({option}) => {
    return option.isMissing ? null : (
    <li key={option.id}>
        <div>
            <h5 className="is-size-5">{option.name}</h5>
            <p>{option.desc}</p>
        </div>
    <h4 className='is-size-5'>${option.price}</h4>
    </li>
    ) ;
}

const Acordion = ({section, sauces, sides, options }) => {
    const [show, setShowing] = useState(false)
    //Toggle handler
    const handleShow = () => setShowing(!show)
    //Class toggler to reflect movement
    const toggleClass = show ? styles['showing'] : styles['hiding'];
    //Id for label and checkbox functionality
    const toggleId = uuid().toString()
    return (
        <AnimatePresence>
        <article>
            <label htmlFor={toggleId} className={styles['label']}>
                <div>
                    <h3 className="title is-size-3">
                        { section.name }
                    </h3>
                    <h4 className='subtitle is-size-5'>
                        { section.desc }
                    </h4>
                </div>
                <span className={toggleClass + ' title is-size-1'}>
                    <input 
                        type="checkbox" 
                        name="toggle" 
                        onChange={ handleShow }
                        checked={ show }
                        id={toggleId}
                    /> +
                </span>
            </label>
            {   // Here we map the options! 
                show && (
                        <motion.ul
                            key={toggleId}
                            initial={{y: -100, opacity: 0, zIndex: -1}}
                            animate={{y: 0, opacity: 1, zIndex: 0}}
                            exit={{y: -200, opacity: 0}}
                            className={styles['options-ul']}
                        >
                        { options.map(option => <ListItem option={option}/>) }
                        { sauces && (
                            <li key={ uuid().toString() }>
                                <div>
                                    <h3 className='subtitle is-size-3'>
                                    {    sauces.name    }
                                    </h3>
                                    {   sauces.options.map(
                                        side => <p key={side.id}>{side.name}</p>
                                    )
                                    }
                                </div>
                            </li>
                        )}                        
                        { sides && (
                            <li key={ uuid().toString() }>  
                                <div>
                                    <h3 className='subtitle is-size-3'>
                                    {    sides.name    }
                                    </h3>
                                    {   sides.options.map(
                                        side => <p key={side.id}>{side.name}</p>
                                    )
                                    }
                                </div>
                            </li>
                        )}
                        </motion.ul>
                )
            }
        </article>
        </AnimatePresence>
    )
}

export default Acordion;
