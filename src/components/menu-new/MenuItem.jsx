import React from 'react';

const MenuItem = ({ option }) => {
  return option.isMissing ? null : (
      <li key={option.id}>
        <div>
          <h5 className="is-size-5">{option.name}</h5>
          <p>{option.desc}</p>
        </div>
        <h4 className='is-size-5'>${option.price}</h4>
      </li>
  );
}
 
export default MenuItem;
