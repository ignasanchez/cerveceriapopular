import firebase from "firebase/app"
import "firebase/auth"
import "firebase/firebase-firestore"
import "firebase/storage"
import "firebase/analytics"

const app = firebase.initializeApp({
  apiKey: "AIzaSyDIsC0LHB62clpbBJag5zX4Xycknd4EZWw",
  authDomain: "cervepopu.firebaseapp.com",
  databaseURL: "https://cervepopu.firebaseio.com",
  projectId: "cervepopu",
  storageBucket: "cervepopu.appspot.com",
  messagingSenderId: "816899978419",
  appId: "1:816899978419:web:74799ed051c697d4fc6cc6",
  measurementId: "G-E4C8TFZ347"
})

firebase.analytics()

export const db = firebase.firestore()
export const storage = firebase.storage()

export default app
