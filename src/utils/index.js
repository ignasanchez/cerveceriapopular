import { findIndex } from 'lodash'

export const specials = [
  {name: "Carnes al Disco"},
  {name: "Pastas al Disco"},
  {name: "Guarniciones del Disco"},
  {name: "Salsas del Disco"}
];

export const sandwiches = [
  {name: "Sandwiches Completos"},
  {name: "Sandwiches Populares"}
];

export const beers =[
  {name: "Cervezas Tiradas"},
  {name: "Cervezas"},
  {name: "Vinos"}
];

export const coffes = [
  {name: "Bebidas Cafetería"},
  {name: "Bebidas sin Alcohol"},
  {name: "Acompañamientos Cafeteria"},
  {name: "Promos Cafetería"}
]

export const suggestion = [{name: "Menu del Día"}]

export const tables = [
  {name: "Tablas de Picada"},
  {name: "Tablas"},
]

/* 
{name: "Ensaladas"},
{ name: "Minutas"},
*/

export function getSpecials (collection) {
  const objects = [];
  specials.forEach(
    special => {
      const index = findIndex(collection, o => o.name === special.name)
      const removedItem = collection.splice(index, 1)
      objects.push(removedItem[0])
    }
  );
  return objects;
}

export function filterCollection (collection, arr) {
  const objects = [];
  arr.forEach(
    el => {
      const index = findIndex(collection, o => o.name === el.name)
      const removedItem = collection.splice(index, 1)
      objects.push(removedItem[0])
    }
  );
  return objects;
}

