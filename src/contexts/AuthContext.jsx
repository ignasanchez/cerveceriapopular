import React, { useState, useEffect } from 'react';
import app from '../firebase';

export const AuthContext = React.createContext();

const AuthContextProvider = ({ children }) => {
    const initial = localStorage.getItem('user') ? 
        JSON.parse( localStorage.getItem('user')) : null;

    const [currentUser, setCurrentUser] = useState(initial)

    const logOut = async (e) => {
        e.preventDefault()
        await app.auth().signOut();
        setCurrentUser(null);
    }

    useEffect(()=> {
        app.auth().onAuthStateChanged(setCurrentUser); 
        const user = JSON.stringify(currentUser);
        localStorage.setItem('user', user)
    }, [currentUser]);

    return (
        <AuthContext.Provider value ={{currentUser, logOut}}>
            { children }
        </AuthContext.Provider>
    );
}
 
export default AuthContextProvider;
